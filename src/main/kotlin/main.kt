
import component.App
import kotlinx.css.*
import react.dom.render
import react.router.dom.hashRouter
import react.router.dom.route
import react.router.dom.switch
import styled.StyledComponents
import kotlin.browser.document

fun main(args: Array<String>) {

    val styles = CSSBuilder().apply {
        html {
            height = 100.pct
        }

        body {
            height = 100.pct
            margin(0.px)
            padding(0.px)
        }
    }
    StyledComponents.injectGlobal(styles.toString())

//    requireAll(require.context("./resources", true, js("/\\.css$/")))

    render(document.getElementById("root")) {
        hashRouter {
            switch {
                route("/", App::class, true)
            }
        }
    }
}
