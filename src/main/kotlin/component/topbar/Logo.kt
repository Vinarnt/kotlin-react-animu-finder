package component.topbar

import kotlinx.css.Color
import kotlinx.css.FontWeight
import kotlinx.css.VerticalAlign
import kotlinx.css.em
import kotlinx.css.properties.LineHeight
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import styled.StyleSheet
import styled.css
import styled.styledSpan

object LogoStyles: StyleSheet("LogoStyles", true) {

    val logo by css {

        color = Color.white
        fontWeight = FontWeight.bold
        fontSize = 2.em
        lineHeight = LineHeight("60px")
        verticalAlign = VerticalAlign.middle
    }
}

class Logo: RComponent<RProps, RState>() {

    override fun RBuilder.render() {

        styledSpan {
            css { +LogoStyles.logo }
            +"Animu Finder"
        }
    }
}

fun RBuilder.logo() = child(Logo::class) {}