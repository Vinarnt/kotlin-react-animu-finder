package component.topbar
import kotlinx.css.Color
import kotlinx.css.LinearDimension
import kotlinx.css.px
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import styled.StyleSheet
import styled.css
import styled.styledDiv

object TopBarStyles: StyleSheet("TopBarStyles", true) {

    val topBar by css {

        backgroundColor = Color("#303030")
        height = 60.px
        width = LinearDimension.available
        padding = "0 10px"
    }
}

class TopBar : RComponent<RProps, RState>() {

    override fun RBuilder.render() {

            styledDiv {

                css { +TopBarStyles.topBar }

                logo()
            }
    }
}

fun RBuilder.topBar() = child(TopBar::class) {}