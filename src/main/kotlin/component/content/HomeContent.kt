package component.content

import kotlinx.css.Color
import kotlinx.css.LinearDimension
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import styled.StyleSheet
import styled.css
import styled.styledDiv

object PopularContentStyles : StyleSheet("PopularContentStyles", true) {

    val wrapper by css {

        backgroundColor = Color("#303030")
        height = LinearDimension("100%")
        width = LinearDimension.available
    }
}

class PopularContent : RComponent<RProps, RState>() {

    override fun RBuilder.render() {

        styledDiv {

            css { +PopularContentStyles.wrapper }

             +"Content here"
        }
    }
}

fun RBuilder.popularContent() = child(PopularContent::class) {}