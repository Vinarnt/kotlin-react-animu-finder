package component
import component.content.popularContent
import component.topbar.topBar
import kotlinx.css.LinearDimension
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import styled.css
import styled.styledDiv

class App : RComponent<RProps, RState>() {

    override fun RBuilder.render() {
        styledDiv {
            css { height = LinearDimension("100%") }

            topBar()
            popularContent()
        }
    }
}
