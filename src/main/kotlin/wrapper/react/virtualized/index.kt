@file:Suppress(
        "INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE",
        "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE",
        "NOTHING_TO_INLINE", "unused")

external interface Index {
    var index: Number
}

external interface PositionInfo {
    var x: Number
    var y: Number
}

external interface ScrollPosition {
    var scrollLeft: Number
    var scrollTop: Number
}

external interface SizeInfo {
    var height: Number
    var width: Number
}

external interface Map<T> {
}

inline operator fun <T> Map<T>.get(key: String): T? = asDynamic()[key] as? T

inline operator fun <T> Map<T>.set(key: String, value: T) {
    asDynamic()[key] = value
}

external interface IndexRange {
    var startIndex: Number
    var stopIndex: Number
}

external interface OverscanIndexRange {
    var overscanStartIndex: Number
    var overscanStopIndex: Number
}

external interface ScrollEventData {
    var clientHeight: Number
    var scrollHeight: Number
    var scrollTop: Number
}
