# Animu Finder

## How to use
### Run with hot reload
``gradlew -t run``

### Stop the server
``gradlew stop``

### Generate the bundle
``gradlew bundle``

## Information
Currently the kotlin dce plugin is not usable, throwing exception preventing the build.